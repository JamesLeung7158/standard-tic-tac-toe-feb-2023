def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

def game_over(*number):
    print_board(board)
    print("GAME OVER")
    print(board[space_number], "has won")
    exit()

def is_row_winner(board, row_number): # row 1 = top, 2 = middle, 3 = bottom
    if row_number == 1:
        row_number = 0;
    if board[(row_number*2)] == board[(row_number*2) + 1] and board[(row_number*2) + 1] == board[(row_number*2 + 2)]:
        return True

def is_column_winner(board, column_number): # column 1 = left, 2 = middle
    if board[(column_number - 1)] == board[(column_number + 2)] and board[(column_number + 2)] == board[(column_number + 5)]:
        return True

def is_diagonal_winner(board, diagonal_number): #diagonal number 1 = left, 2 = right
    if board[(diagonal_number - 1)*2] == board[4] and board[4] == board[10 - (diagonal_number*2)]:
        return True

def top_row_is_winner(board):
    if board[0] == board[1] and board[1] == board[2]:
        return True
def middle_row_is_winner(board):
    if board[4] == board[5] and board[5] == board[6]:
        return True
def bottom_row_is_winner(board):
    if board[6] == board[7] and board[7] == board[8]:
        return True
def left_column_is_winner(board):
    if board[0] == board[3] and board[3] == board[6]:
        return True
def middle_column_is_winner(board):
    if board[1] == board[4] and board[4] == board[7]:
        return True
def right_column_is_winner(board):
    if board[2] == board[5] and board[5] == board[8]:
        return True
def left_diagonal_is_winner(board):
    if board[0] == board[4] and board[4] == board[8]:
        return True
def right_diagonal_is_winner(board):
    if board[2] == board[4] and board[4] == board[6]:
        return True



board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if is_row_winner(board, 1):
        game_over(board);
    elif is_row_winner(board, 2):
        game_over(board);
    elif is_row_winner(board, 3):
        game_over(board);
    elif is_column_winner(board, 1):
        game_over(board);
    elif is_column_winner(board, 2):
        game_over(board);
    elif is_column_winner(board, 3):
        game_over(board);
    elif is_diagonal_winner(board, 1):
        game_over(board);
    elif is_diagonal_winner(board, 2):
        game_over(board);

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
